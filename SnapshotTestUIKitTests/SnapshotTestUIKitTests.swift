//
//  SnapshotTestUIKitTests.swift
//  SnapshotTestUIKitTests
//
//  Created by Anton Mikhailitsyn on 02.08.2023.
//

import XCTest
import iOSSnapshotTestCase
@testable import SnapshotTestUIKit

final class SnapshotTestUIKitTests: FBSnapshotTestCase {
    
    var vc : UIViewController!

    override func setUp() {
        super.setUp()
        self.recordMode = false
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        vc = storyboard.instantiateInitialViewController()
    }
    
    func testCase() {
        FBSnapshotVerifyView(vc.view)
    }
    
    override func tearDown() {
        super.tearDown()
        vc = nil
        
    }
}
